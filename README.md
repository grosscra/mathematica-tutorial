# Mathematica Tutorial

Files for Mathematica tutorial given by MSU AMS graduate student chapter.

The main file is `Tutorial.nb` which includes the input in each lesson. The blanked file `Tutorial_Blank.nb` removes this input, e.g., for presentations or a space to copy the input into. The file `Tutorial_Solutions.nb` includes solutions to all exercises.
